# ARR - Elevator Package

This is my custom package that allows for the elevation of privileges on a linux machine.

## Requirements

* `user` - Must be installed before this one.

## Install - Local

Run the following command from within this directory:

``` bash
$ ./install.sh
```

## Uninstall - Local

Run the following command:

``` bash
$ ./uninstall.sh
```

