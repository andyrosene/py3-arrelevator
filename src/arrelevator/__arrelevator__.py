from getpass import getpass
from os import system
from arruser import get_username

__all__ = [
    'elevate',
    'release'
]


def elevate(reason: str):
    """Acquire/Renew elevated privileges.

    Args:
        reason: The reason that permissions are being elevated.
    """
    # Attempt to renew the existing permissions
    if __login():
        return

    print(reason)
    username = get_username()

    while True:
        password = getpass(prompt='[sudo] password for {}: '.format(username))
        if __login(password):
            break


def release():
    """Exit the elevated permissions execution state. Without calling this, you can get stuck in an elevated state and
    cause more harm than good.
    """
    system('sudo -k >/dev/null 2>/dev/null')


def __login(password: str = '') -> bool:
    """Attempt to login using the given password. If no password is given the empty string is used as a means of
    checking if currently logged in and also renewing the existing login.

    fixme - this function needs to be renamed to something that better describes what it is actually doing.

    Args:
        password: the given password.

    Returns:
        bool: Whether the escalated permissions are applied or not.
    """
    return system('echo {} | sudo -Sl >/dev/null 2>/dev/null'.format(password)) == 0
